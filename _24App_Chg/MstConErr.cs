﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24App_Chg
{
	public class MstConErrMerGr
	{
		private int _Mt_Con_Err_Mercchant_Group_Id = 0;
		private int _Mt_Merchant_Group_Id = 0;
		private int _Mt_Merchant_Group_Id_Err = 0;

		public int Mt_Con_Err_Mercchant_Group_Id { get { return _Mt_Con_Err_Mercchant_Group_Id; } set { _Mt_Con_Err_Mercchant_Group_Id = value; } }
		public int Mt_Merchant_Group_Id { get { return _Mt_Merchant_Group_Id; } set { _Mt_Merchant_Group_Id = value; } }
		public int Mt_Merchant_Group_Id_Err { get { return _Mt_Merchant_Group_Id_Err; } set { _Mt_Merchant_Group_Id_Err = value; } }
	}

	public class MstConErr
	{
		private int mt_con_err_merchant_id = 0;
		private int mt_merchant_id = 0;
		private int mt_merchant_id_pv_err = 0;
		private int mt_merchant_id_aw_err = 0;
		private int mt_merchant_id_wp_err = 0;
		private byte flg_con_err_type_current = 0;
		private DateTime _Date_Addnew = new DateTime();
		private DateTime _Date_Lastup = new DateTime();
		private DateTime _Date_Change = new DateTime();

		public int Mt_Merchant_Id { get { return mt_merchant_id; } set { mt_merchant_id = value; } }
		public int Mt_Merchant_id_WP_Err { get { return mt_merchant_id_wp_err; } set { mt_merchant_id_wp_err = value; } }
		public int Mt_Merchant_id_AW_Err { get { return mt_merchant_id_aw_err; } set { mt_merchant_id_aw_err = value; } }
		public int Mt_Merchant_id_PV_Err { get { return mt_merchant_id_pv_err; } set { mt_merchant_id_pv_err = value; } }
		public byte Flg_Con_Err_Type_Current { get { return flg_con_err_type_current; } set { flg_con_err_type_current = value; } }
		public DateTime Date_Change { get { return _Date_Change; } set { _Date_Change = value; } }
		public DateTime Date_Lastup { get { return _Date_Lastup; } set { _Date_Lastup = value; } }
		public DateTime Date_Addnew { get { return _Date_Addnew; } set { _Date_Addnew = value; } }
	}

	public class DBAccess
	{
		private readonly string conString = ConfigurationManager.ConnectionStrings["ConnString1"].ConnectionString;



		/// <summary>
		/// 変更MIDを全ブランド更新
		/// </summary>
		/// <param name="merid"></param>
		/// <param name="merid_err"></param>
		/// <returns></returns>
		internal int edt_merchant_group(int merid, int merid_err)
		{
			int nRet = 0;
			using (SqlConnection sqlCon = new SqlConnection(conString))
			{
				using (SqlCommand SqlCmd = new SqlCommand())
				{
					SqlCmd.Connection = sqlCon;
					SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
					SqlCmd.CommandText = "tf_edt_merchant_group2";
					SqlCmd.Parameters.Add(new SqlParameter("@merid", merid));
					SqlCmd.Parameters.Add(new SqlParameter("@merid_err", merid_err));

					if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
					nRet= SqlCmd.ExecuteNonQuery();
				}
			}
			return nRet;
		}

		/// <summary>
		/// 指定接続先をもつMerchantGroupを取得
		/// </summary>
		/// <param name="flg_div"></param>
		/// <returns></returns>
		internal DataTable GetMstMerchantErr(byte flg_div)
		{
			DataTable dt = new DataTable();

			using (SqlConnection sqlCon = new SqlConnection(conString))
			{
				using (SqlCommand SqlCmd = new SqlCommand())
				{
					SqlCmd.Connection = sqlCon;
					SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
					SqlCmd.CommandText = "tf_lst_merchant_err";
					SqlCmd.Parameters.Add(new SqlParameter("@flg_con_division", flg_div));

					if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
					SqlDataReader data_reader = SqlCmd.ExecuteReader();
					dt.Load(data_reader);
				}
			}
			return dt;
		}

		/// <summary>
		/// 指定接続先を持つデータの変更日時を更新
		/// </summary>
		/// <param name="mt_merchant_err_id"></param>
		/// <returns></returns>
		internal int EdtMstMerchantErr(int mt_merchant_err_id)
		{
			int nRet = 0;
			using (SqlConnection sqlCon = new SqlConnection(conString))
			{
				using (SqlCommand SqlCmd = new SqlCommand())
				{
					SqlCmd.Connection = sqlCon;
					SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
					SqlCmd.CommandText = "tf_edt_merchant_err";
					SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_err_id", mt_merchant_err_id));

					if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
					nRet = SqlCmd.ExecuteNonQuery();
					
				}
			}
			return nRet;
		}
	}
}
