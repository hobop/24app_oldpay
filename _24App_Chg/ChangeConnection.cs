﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using _24App_Chg;
using System.Diagnostics;
using System.Collections;

namespace _24App
{

	public enum flg_con_division
	{
		/// <summary>正常:0</summary>
		OK = 0,

		/// <summary>Telecom:1</summary>
		telecom = 1,
		/// <summary>Bibit1:10</summary>
		worldpay1 = 10,
		/// <summary>Payvision:13</summary>
		payvision = 13,
		/// <summary>AlliedWallet:17</summary>
		alliedwallet = 17,
		/// <summary>Bibit2:18</summary>
		worldpay2 = 18,
	}

	public class ChangeConnection
	{
		public static void Run(flg_con_division con)
		{
			string conString = ConfigurationManager.ConnectionStrings["ConnString1"].ConnectionString;
			byte div;
			switch (con)
			{
				
				case flg_con_division.payvision: div = 1; break;
				case flg_con_division.worldpay1: div = 2; break;
				case flg_con_division.worldpay2: div = 2; break;
				case flg_con_division.alliedwallet: div = 3; break;
				case flg_con_division.telecom: div = 4; break;
				case flg_con_division.OK: div = 0; break;
				default:throw new Exception();
			}

			DataTable dt = new DataTable();

			using (SqlConnection sqlCon = new SqlConnection(conString))
			{
				using (SqlCommand SqlCmd = new SqlCommand())
				{
					SqlCmd.Connection = sqlCon;
					SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
					SqlCmd.CommandText = "tf_lst_con_err_merchant_group_err_type";
					SqlCmd.Parameters.Add(new SqlParameter("@flg_con_error_type", div));


					if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
					SqlDataReader data_reader = SqlCmd.ExecuteReader();
					dt.Load(data_reader);
				}
			}

			foreach (DataRow item in dt.Rows)
			{
				using (SqlConnection sqlCon = new SqlConnection(conString))
				{
					using (SqlCommand SqlCmd = new SqlCommand("tf_edt_merchant_group", sqlCon))
					{
						SqlCmd.CommandType = CommandType.StoredProcedure;
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_visa", (int)item["mt_merchant_id_visa"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_master", (int)item["mt_merchant_id_master"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_jcb", (int)item["mt_merchant_id_jcb"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_amex", (int)item["mt_merchant_id_amex"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_diners", (int)item["mt_merchant_id_diners"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_id_other", (int)item["mt_merchant_id_other"]));
						SqlCmd.Parameters.Add(new SqlParameter("@mt_merchant_group_id", (int)item["mt_merchant_group_id"]));


						if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
						SqlCmd.ExecuteNonQuery();
					}
				}
			}
			//カレントの変更
			using (SqlConnection sqlCon = new SqlConnection(conString))
			{
				using (SqlCommand SqlCmd = new SqlCommand("tf_edt_con_err_merchant_group_current", sqlCon))
				{
					SqlCmd.CommandType = CommandType.StoredProcedure;
					SqlCmd.Parameters.Add(new SqlParameter("@flg_con_error_type_current", div));

					if (SqlCmd.Connection.State == System.Data.ConnectionState.Closed) SqlCmd.Connection.Open();
					SqlCmd.ExecuteNonQuery();
				}
			}

		}



		internal struct chg_struct
		{
			internal string st_name;
			internal string id;
			internal string id_org;
		}

		internal struct mst_merchant_group
		{
			internal int mid_visa;
			internal int mid_master;
			internal int mid_jcb;
			internal int mid_amex;
			internal int mid_diners;
			internal int mid_other;
		}

		internal static List<chg_struct> _ChgList = new List<chg_struct>();
		internal static List<chg_struct> ChgList
		{
			get
			{
				if (_ChgList != null && _ChgList.Count != 0) return _ChgList;
				_ChgList = new List<chg_struct>();


				chg_struct tmp = new chg_struct();
				tmp.id = "@visa";
				tmp.id_org = "@visa_org";
				tmp.st_name = "tf_chg_merchant_visa";
				_ChgList.Add(tmp);
				tmp.id = "@master";
				tmp.id_org = "@master_org";
				tmp.st_name = "tf_chg_merchant_master";
				_ChgList.Add(tmp);
				tmp.id = "@jcb";
				tmp.id_org = "@jcb_org";
				tmp.st_name = "tf_chg_merchant_jcb";
				_ChgList.Add(tmp);
				tmp.id = "@amex";
				tmp.id_org = "@amex_org";
				tmp.st_name = "tf_chg_merchant_amex";
				_ChgList.Add(tmp);
				tmp.id = "@diners";
				tmp.id_org = "@diners_org";
				tmp.st_name = "tf_chg_merchant_diners";
				_ChgList.Add(tmp);
				tmp.id = "@other";
				tmp.id_org = "@other_org";
				tmp.st_name = "tf_chg_merchant_other";
				_ChgList.Add(tmp);


				return _ChgList;
			}
		}

		public static void Run2(flg_con_division flg_con_division)
		{
			try
			{
				//切り戻し、Telecomはやらない。
				if (flg_con_division == flg_con_division.telecom || flg_con_division == flg_con_division.OK) return;

				string conString = ConfigurationManager.ConnectionStrings["ConnString1"].ConnectionString;
				byte div_val;
				switch (flg_con_division)
				{
					case flg_con_division.payvision: div_val = 1; break;
					case flg_con_division.worldpay1: div_val = 2; break;
					case flg_con_division.worldpay2: div_val = 2; break;
					case flg_con_division.alliedwallet: div_val = 3; break;
					case flg_con_division.telecom: div_val = 4; break;
					case flg_con_division.OK: div_val = 0; break;
					default: throw new Exception();
				}

				DBAccess db = new DBAccess();
				DataTable dt_mserchant_err = db.GetMstMerchantErr(div_val);
				//int nResult_Edt = db.EdtMstMerchantErr(div_val);

				List<int> updated_list = new List<int>();
				Dictionary<int, List<int>> updated_list_brands = new Dictionary<int, List<int>>();
				foreach (DataRow item in dt_mserchant_err.Rows)
				{

					int mt_merchant_id = 0;     //切替元
					int mt_merchant_id_err = 0; //切替先
					int mt_merchant_err_id = 0; //エラーマスタID

					mt_merchant_id = int.Parse(item["Mt_Merchant_Id"].ToString());
					mt_merchant_id_err = int.Parse(item["Mt_Merchant_Id_Err"].ToString());
					mt_merchant_err_id = int.Parse(item["Mt_Merchant_Err_Id"].ToString());

					if (updated_list.Contains(mt_merchant_id)) continue;


					int nResult = db.edt_merchant_group(mt_merchant_id, mt_merchant_id_err);
					int nResult_Edt = db.EdtMstMerchantErr(mt_merchant_err_id);

					updated_list.Add(mt_merchant_id);

					try
					{
						OutputLog(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " [" + mt_merchant_id + "] to [" + mt_merchant_id_err + "]" + Environment.NewLine);
					}
					catch(Exception ex)
					{
						OutputLog("ログ出力エラー" + Environment.NewLine + ex.Message);
					}
				}
			}
			catch(Exception ex)
			{
				OutputLog(ex.Message);
			}
		}


		private static void OutputLog(string msg)
		{
			string filepath = @"D:\_App24_v2\Log";
			string filename = DateTime.Now.ToString("yyyyMMdd") + ".log";
			filepath = Path.Combine(filepath, filename);
			string out_msg = "";
			out_msg += Environment.NewLine;
			out_msg += "========================================";
			out_msg += Environment.NewLine + msg + Environment.NewLine;
			out_msg += "========================================";
			out_msg += Environment.NewLine;

			File.AppendAllText(filepath, out_msg, Encoding.UTF8);
		}
	}
}
