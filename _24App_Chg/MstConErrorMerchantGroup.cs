﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _24App
{
	public class MstConErrorMerchantGroup
	{
		private int mMt_Con_Error_Merchant_Group_Id = 0;
		private int mMt_Merchant_Group_Id = 0;
		private byte mFlg_Con_Error_Type = 0;
		private int mMt_Merchant_Id_Visa = 0;
		private int mMt_Merchant_Id_Master = 0;
		private int mMt_Merchant_Id_Jcb = 0;
		private int mMt_Merchant_Id_Amex = 0;
		private int mMt_Merchant_Id_Diners = 0;
		private int mMt_Merchant_Id_Other = 0;
		private byte mFlg_Con_Error_Type_Current = 0;
		DateTime mDate_Addnew = new DateTime(2000, 1, 1);
		DateTime mDate_Lastup = new DateTime(2000, 1, 1);
		DateTime mDate_Change = new DateTime(2000, 1, 1);


		string mMerchant_Code_Visa = "";
		string mMerchant_Code_Master = "";
		string mMerchant_Code_Jcb = "";
		string mMerchant_Code_Amex = "";
		string mMerchant_Code_Diners = "";
		string mMerchant_Code_Other = "";
		string mMerchant_Group_Code = "";

		public DateTime Date_Change
		{
			get
			{
				return mDate_Change;
			}

			set
			{
				mDate_Change = value;
			}
		}

		public DateTime Date_Lastup
		{
			get
			{
				return mDate_Lastup;
			}

			set
			{
				mDate_Lastup = value;
			}
		}

		public DateTime Date_Addnew
		{
			get
			{
				return mDate_Addnew;
			}

			set
			{
				mDate_Addnew = value;
			}
		}

		public byte Flg_Con_Error_Type_Current
		{
			get
			{
				return mFlg_Con_Error_Type_Current;
			}

			set
			{
				mFlg_Con_Error_Type_Current = value;
			}
		}

		public int Mt_Merchant_Id_Other
		{
			get
			{
				return mMt_Merchant_Id_Other;
			}

			set
			{
				mMt_Merchant_Id_Other = value;
			}
		}

		public int Mt_Merchant_Id_Diners
		{
			get
			{
				return mMt_Merchant_Id_Diners;
			}

			set
			{
				mMt_Merchant_Id_Diners = value;
			}
		}

		public int Mt_Merchant_Id_Amex
		{
			get
			{
				return mMt_Merchant_Id_Amex;
			}

			set
			{
				mMt_Merchant_Id_Amex = value;
			}
		}

		public int Mt_Merchant_Id_Jcb
		{
			get
			{
				return mMt_Merchant_Id_Jcb;
			}

			set
			{
				mMt_Merchant_Id_Jcb = value;
			}
		}

		public int Mt_Merchant_Id_Master
		{
			get
			{
				return mMt_Merchant_Id_Master;
			}

			set
			{
				mMt_Merchant_Id_Master = value;
			}
		}

		public int Mt_Merchant_Id_Visa
		{
			get
			{
				return mMt_Merchant_Id_Visa;
			}

			set
			{
				mMt_Merchant_Id_Visa = value;
			}
		}

		public byte Flg_Con_Error_Type
		{
			get
			{
				return mFlg_Con_Error_Type;
			}

			set
			{
				mFlg_Con_Error_Type = value;
			}
		}

		public int Mt_Merchant_Group_Id
		{
			get
			{
				return mMt_Merchant_Group_Id;
			}

			set
			{
				mMt_Merchant_Group_Id = value;
			}
		}

		public int Mt_Con_Error_Merchant_Group_Id
		{
			get
			{
				return mMt_Con_Error_Merchant_Group_Id;
			}

			set
			{
				mMt_Con_Error_Merchant_Group_Id = value;
			}
		}

		public string Merchant_Group_Code
		{
			get
			{
				return mMerchant_Group_Code;
			}

			set
			{
				mMerchant_Group_Code = value;
			}
		}

		public string Merchant_Code_Other
		{
			get
			{
				return mMerchant_Code_Other;
			}

			set
			{
				mMerchant_Code_Other = value;
			}
		}

		public string Merchant_Code_Diners
		{
			get
			{
				return mMerchant_Code_Diners;
			}

			set
			{
				mMerchant_Code_Diners = value;
			}
		}

		public string Merchant_Code_Amex
		{
			get
			{
				return mMerchant_Code_Amex;
			}

			set
			{
				mMerchant_Code_Amex = value;
			}
		}

		public string Merchant_Code_Jcb
		{
			get
			{
				return mMerchant_Code_Jcb;
			}

			set
			{
				mMerchant_Code_Jcb = value;
			}
		}

		public string Merchant_Code_Master
		{
			get
			{
				return mMerchant_Code_Master;
			}

			set
			{
				mMerchant_Code_Master = value;
			}
		}

		public string Merchant_Code_Visa
		{
			get
			{
				return mMerchant_Code_Visa;
			}

			set
			{
				mMerchant_Code_Visa = value;
			}
		}
	}
}