﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Security;

namespace _App24Pay.Connection
{

	public enum ConnectionMethod
	{
		/// <summary>-1</summary>
		None = -1,
		/// <summary>0</summary>
		WP = 0,
		/// <summary>1</summary>
		PV = 1,
		/// <summary>2</summary>
		AW = 2,
	}

	/// ===============================================================================================================
	/// ■機　能：<summary>Connectionクラスにおけるサーバ接続時の接続方法を指定します。</summary>
	/// 
	/// ■備　考：2009/10/21 nakayama	新規作成
	/// ===============================================================================================================
	public enum ConnectionRequestType
	{

		/// <summary>この接続はGETで行います。</summary>
		Get = 1,
		/// <summary>この接続はPOSTで行います。</summary>
		Post = 2,
		/// <summary>この接続は画面遷移(リダイレクト)を行います。</summary>
		Redirect = 3,

	} //== End of ConnectionRequestType enum ==//

	/// ===============================================================================================================
	/// ■機　能：<summary>Connectionクラスにおけるサーバ接続時に実行するオプション動作を指定します。</summary>
	/// 
	/// ■備　考：2009/10/22 nakayama	新規作成
	/// ===============================================================================================================
	public enum ConnectionRequestOption
	{

		/// <summary>この接続は通常通りの送信を行います。</summary>
		Normal = 0,
		/// <summary>この接続はファイルデータを添付して送信します。</summary>
		File = 1,
		/// <summary>この接続はXMLデータで送信します。</summary>
		XmlData = 2,

	} //== End of ConnectionRequestOption enum ==//

	/// ===============================================================================================================
	/// ■機　能：<summary>Connectionクラスにおけるサーバ受信時に実行するオプション動作を指定します。</summary>
	/// ===============================================================================================================
	public enum ConnectionResponseOption
	{

		/// <summary>この接続は通常通りの受信を行います。</summary>
		Normal = 0,
		/// <summary>この接続はXMLデータで受信します。</summary>
		XmlData = 2,

	} //== End of ConnectionResponseOption enum ==//


	public class _Connection_Util
	{

		/****************************************************************************************************/
		/* プライベート変数定義																				*/
		/****************************************************************************************************/
		
		/// <summary>ソケット通信用クライアントです。</summary>
		public TcpClient mTcp_Client = null;
		//public	Socket						mTcp_Client				= null;
		/// <summary>ソケット通信で使用するネットワークストリームクラスです。</summary>
		public NetworkStream mTcp_Stream = null;
		/// <summary>ソケット通信で接続するサーバ名です。</summary>
		public string mTcp_Ip = "127.0.0.1";
		/// <summary>ソケット通信で接続するポート番号です。</summary>
		public int mTcp_Port = 80;
		/// <summary>ソケット通信で使用する文字コードタイプです。</summary>
		public Encoding mTcp_Encoding = Encoding.UTF8;
		/// <summary>現在のテストモードの有効状態です。</summary>
		public bool mIs_Test_Mode = false;
		/// <summary>接続時に送信するContentTypeです。</summary>
		public string mContent_Type = "application/x-www-form-urlencoded";
		/// <summary>接続先からのリダイレクト応答に自動的に従うかどうかを示します。</summary>
		public bool mAllow_Auto_Redirect = true;
		/// <summary>接続時に送信するUserAgentです。</summary>
		public string mUser_Agent = "";
		/// <summary>接続時のタイムアウト秒数です。(単位：ミリ秒)</summary>
		public int mTime_Out = 180000;
		/// <summary>接続時の接続方法です。</summary>
		public ConnectionRequestType mRequest_Type = 0;
		/// <summary>接続先名です。</summary>
		public string mConnect_Name = "";
		/// <summary>接続時のオプション動作です。</summary>
		public ConnectionRequestOption mRequest_Option = 0;
		/// <summary>接続先のURLです。</summary>
		public string mRequest_Url = "";
		/// <summary>接続先にデータを送信する際の文字コードです。接続方法がPOST時のみに使用します。</summary>
		public Encoding mRequest_Encoding = Encoding.ASCII;
		/// <summary>接続先で認証が必要な際に送信する、ユーザID情報です。</summary>
		public string mRequest_User = "";
		/// <summary>接続先で認証が必要な際に送信する、パスワード情報です。</summary>
		public string mRequest_Pass = "";
		/// <summary>接続先に送信するデータ内容です。</summary>
		public StringBuilder mRequest_Data = new StringBuilder();
		/// <summary>接続先に添付ファイルデータを送信する場合の、ファイル名です。</summary>
		public string mRequest_File_Name = "";
		/// <summary>接続先に添付ファイルデータを送信する場合の、ファイルデータ内容です。</summary>
		public StringBuilder mRequest_File_Data = new StringBuilder();
		/// <summary>接続先から受信するさいのオプション動作です。</summary>
		public ConnectionResponseOption mResponse_Option = 0;
		/// <summary>接続先から受信したデータ内容です。</summary>
		public StringBuilder mResponse_Data = new StringBuilder();
		/// <summary>接続先から受信したXMLデータ内容です。オプション動作がXMLデータ時のみに使用します。</summary>
		public XmlDocument mResponse_Xml_Data = new XmlDocument();
		public StringBuilder mRequest_Log = new StringBuilder();
		public WebHeaderCollection mResponse_Headers = null;
		public StringBuilder mResponse_Log = new StringBuilder();
		public byte mFlg_Request_No = 1;
		public byte mFlg_Response_No = 1;

		/// <summary>追加ヘッダー情報「Authorization」</summary>
		public string mAuthorization = "";
		/// <summary>追加ヘッダー情報「Accept」</summary>
		protected string mAccept = "";


		private HttpStatusCode mStatusCode;
		public HttpStatusCode StatusCode { get { return this.mStatusCode; } set { this.mStatusCode = value; } }

		public void ConnectHttpRequest()
		{


			// 受信データの初期化
			this.mResponse_Data = new StringBuilder();
			this.mResponse_Xml_Data = new XmlDocument();

			// 送信データの加工
			
			StringBuilder send_url = new StringBuilder(this.mRequest_Url);
			byte[] send_data_post = null;
			byte[] send_data_file = new byte[0];
			byte[] send_data_end = new byte[0];
			try
			{
				switch (this.mRequest_Type)
				{
					case ConnectionRequestType.Get: // GET送信
						// 接続先URLにパラメータ情報を付加
						if (this.mRequest_Data.Length > 0) send_url.Append("?" + this.mRequest_Data.ToString());
						break;
					case ConnectionRequestType.Post: // POST送信
						// 送信用データの加工処理
						switch (this.mRequest_Option)
						{
							case ConnectionRequestOption.File: // ファイルデータ添付の場合
								// 区切り文字列の生成
								string boundary = System.Environment.TickCount.ToString();
								// Content-Typeの変更
								this.mContent_Type = "multipart/form-data; boundary=" + boundary;
								// 送信ファイル名の生成(未指定の場合)
								if (this.mRequest_File_Name == "") this.mRequest_File_Name = DateTime.Now.ToString("yyyyMMdd") + boundary + ".txt";
								// 送信データ部分作成
								StringBuilder post_data = new StringBuilder();
								foreach (string parms in this.mRequest_Data.ToString().Split('&'))
								{
									string[] parm = parms.Split('=');
									post_data.Append("--" + boundary + "\r\n");
									post_data.Append("Content-Disposition: form-data; name=\"" + parm[0] + "\"\r\n\r\n");
									post_data.Append(parm[1] + "\r\n");
								}
								// 送信ファイル部分作成
								post_data.Append("--" + boundary + "\r\n");
								post_data.Append("Content-Disposition: form-data; name=\"refund_file\"; filename=\"" + this.mRequest_File_Name + "\"\r\n");
								post_data.Append("Content-Type: application/octet-stream\r\n");
								post_data.Append("Content-Transfer-Encoding: binary\r\n\r\n");
								// 送信用データを上書き
								this.mRequest_Data = post_data;
								// 送信ファイルデータをバイト型配列に変換
								send_data_file = this.mRequest_Encoding.GetBytes(this.mRequest_File_Data.ToString());
								// 終了文字列をバイト型配列に変換
								send_data_end = this.mRequest_Encoding.GetBytes("\r\n--" + boundary + "--\r\n");
								break;
							case ConnectionRequestOption.XmlData: // XMLデータの場合
								// Content-Typeの変更
								this.mContent_Type = "text/xml";
								break;
						}
						// 送信データをバイト型配列に変換
						send_data_post = this.mRequest_Encoding.GetBytes(this.mRequest_Data.ToString());
						break;
					default: // その他
						throw new Exception();
				}
			}
			catch (Exception ex)
			{ }

			// HttpWebRequest生成処理
			HttpWebRequest request = null;

			//////////////////////////////////////////////////////////////////////////////////////////
			System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
			//////////////////////////////////////////////////////////////////////////////////////////


			try
			{
				// 証明書ポリシーを上書き(オレオレ証明書対策)
				ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(_Connection_Util.RemoteCertificateValidationCallback);
				// HttpWebRequest生成
				request = (HttpWebRequest)WebRequest.Create(send_url.ToString());
				if (this.mRequest_Type == ConnectionRequestType.Post)
				{
					// POST送信時の追加設定
					request.Method = "POST";
					request.ContentType = this.mContent_Type;
					request.ContentLength = send_data_post.Length + send_data_file.Length + send_data_end.Length;
					if (this.mRequest_User != "")
					{
						// 認証設定が存在する場合
						request.Credentials = new NetworkCredential(this.mRequest_User, this.mRequest_Pass);
					}
				}
				request.AllowAutoRedirect = this.mAllow_Auto_Redirect;
				request.UserAgent = this.mUser_Agent;
				request.Timeout = this.mTime_Out;



				//if (this.mController.Parms.Ss_Service_Id == 11)
				//{
				//    request.Headers["Authorization"] = "Basic " + this.mAuthorization;
				//    request.Accept = this.mAccept;
				//}

			}
			catch (NotSupportedException ex) { throw ex; }
			catch (ArgumentNullException ex) { throw ex; }
			catch (SecurityException ex){ throw ex; }
			catch (UriFormatException ex) { throw ex; }
			catch (Exception ex) { throw ex; }
			// POSTデータの送信
			if (this.mRequest_Type == ConnectionRequestType.Post)
			{
				Stream stream = null;
				try
				{
					// 送信用ストリームの生成
					stream = request.GetRequestStream();
					// POSTデータ送信
					stream.Write(send_data_post, 0, send_data_post.Length);
					stream.Write(send_data_file, 0, send_data_file.Length);
					stream.Write(send_data_end, 0, send_data_end.Length);
					stream.Close();
				}
				catch (ProtocolViolationException ex) { throw ex; }
				catch (WebException ex) { throw ex; }
				catch (InvalidOperationException ex) { throw ex; }
				catch (Exception ex) { throw ex; }
			}

			// HttpWebResponse生成処理
			HttpWebResponse response = null;
			try
			{
				// HttpWebResponse生成
				response = (HttpWebResponse)request.GetResponse();
				if (response == null) throw new Exception("response is null.");
			}
			catch (ProtocolViolationException ex) { throw ex; }
			catch (WebException ex) { throw ex; }
			catch (InvalidOperationException ex) { throw ex; }
			catch (Exception ex) { throw ex; }

			this.mStatusCode = response.StatusCode;

			// レスポンスデータ取得
			if (response.ContentLength != 0)
			{
				try
				{
					// 受信用ストリームのチェック
					Stream stream = response.GetResponseStream();
					if (stream.CanRead == false)
					{
						// 受信用ストリームが読取りをサポートしていないはずが無いので、エラーとする
						stream.Close();
						// エラーログを記録して例外処理
						throw new Exception();
					}
					// Locationヘッダの取得
					try
					{
						this.mResponse_Headers = response.Headers;
					}
					catch (Exception ex)
					{
					}
					// データ受信処理
					if (this.mResponse_Option != ConnectionResponseOption.XmlData)
					{
						// データがXMLでは無い場合は、受信用ストリームを生成
						StreamReader stm_reader = null;
						TextReader txt_reader = null;
						try
						{
							// 受信用ストリーム生成
							stm_reader = new StreamReader(stream, Encoding.Default);
							txt_reader = (TextReader)stm_reader;
						}
						catch (Exception ex)
						{
						
							// 念の為、受信用ストリームを閉じる
							try { stm_reader.Close(); }
							catch {	/* 何もしない*/		}
							// エラーログを記録して例外処理
						
						
							throw new Exception();
						}
						// レスポンスデータの取得
						try
						{
							// データを全て取得
							while (txt_reader.Peek() > -1)
							{
								string line = txt_reader.ReadLine();
								this.mResponse_Data.Append((line != null ? line : ""));
							}
						}
						catch (Exception ex)
						{
						}
						finally
						{
							// 受信用ストリームを閉じる
							try { stm_reader.Close(); }
							catch {	/* 何もしない*/		}
						}
					}
					else
					{
						// データがXMLの場合は、XmlDocumentで読取
						try
						{
							try
							{
								if (this.mResponse_Option == ConnectionResponseOption.XmlData)//XMLの場合はとりにいかない、ように。
								{
									this.mResponse_Xml_Data.XmlResolver = null;
									this.mResponse_Xml_Data.Load(stream);
								}
								else
								{
									this.mResponse_Xml_Data.Load(stream);
								}
							}
							catch { }
						}
						catch (Exception ex) { throw ex; }
					}
				}
				catch (ProtocolViolationException ex) { throw ex; }
				catch (Exception ex) { throw ex; }
				finally
				{
					// 念の為、HttpWebResponse を閉じておく
					try { response.Close(); }
					catch {	/* 何もしない */	}
				}
			}
			else if (response.ContentLength == 0 && this.mAllow_Auto_Redirect == false)
			{
				try
				{
					this.mResponse_Headers = response.Headers;
				}
				catch (Exception ex) { throw ex; }
				finally
				{
					// 念の為、HttpWebResponse を閉じておく
					try { response.Close(); }
					catch {	/* 何もしない */	}
				}
			}

			try
			{
				// 受信ログの保存
				if (this.mResponse_Option == ConnectionResponseOption.XmlData)
				{//xmlの場合保存されない場合があるので格納する。
					this.mResponse_Data = new StringBuilder(this.mResponse_Xml_Data.OuterXml);
				}
			}
			catch { }
			

		}

		public static bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors ssl_policy_errors)
		{

			// 問答無用で有効とする
			return true;

		}



		
	}
}
