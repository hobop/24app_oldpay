﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;

namespace _App24Pay.Connection
{
	public class Worldpay:_IConnection
	{
		private _Connection_Util con = null;

		public bool Run()
		{
			try
			{
				SetConnection_Util();


				XmlElement elm = null;
				string data_dir =  ConfigurationManager.AppSettings["data_dir"];
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.XmlResolver = null;
				xmlDoc.Load(Path.Combine(data_dir, "wp.xml"));
				
				elm = xmlDoc["paymentService"]["submit"]["order"]["paymentDetails"]["VISA-SSL"]["expiryDate"]["date"];
				elm.SetAttribute("month", DateTime.Now.AddMonths(1).Month.ToString("00"));
				elm.SetAttribute("year", DateTime.Now.AddMonths(1).Year.ToString());

				elm = xmlDoc["paymentService"]["submit"]["order"];
				elm.SetAttribute("orderCode", DateTime.Now.ToString("HHmmssfff"));

				con.mRequest_Data = new System.Text.StringBuilder(xmlDoc.InnerXml);
				con.ConnectHttpRequest();


				bool ret = !string.IsNullOrEmpty(con.mResponse_Xml_Data.InnerXml);

				if (ret && con.StatusCode != System.Net.HttpStatusCode.OK)
				{
					ret = false;
				}

				//ここまできて中身チェック
				if (ret)
				{
					try
					{
						string code = con.mResponse_Xml_Data["paymentService"]["reply"]["payment"]["ISO8583ReturnCode"].GetAttribute("code");
						if (!string.IsNullOrEmpty(code) && (code == "3" || code == "20"))
						{
							ret = false;
						}
					}
					catch { }
				}
				SetResult(ret);
				CheckResult();
				return ret;
			}
			catch(Exception ex)
			{
				SetResult(false);
				CheckResult();
				return false;
			}
			
		}

		public void SetConnection_Util()
		{
			con = new _Connection_Util();
			con.mRequest_Url = ConfigurationManager.AppSettings["wp_url"];
			con.mRequest_Type = ConnectionRequestType.Post;
			con.mRequest_Option = ConnectionRequestOption.XmlData;
			con.mResponse_Option = ConnectionResponseOption.XmlData;
			con.mRequest_User = ConfigurationManager.AppSettings["wp_code"];
			con.mRequest_Pass = ConfigurationManager.AppSettings["wp_pass"];
			con.mTime_Out = 10000;
		}



		#region _IConnection メンバ


		public void SetResult(bool b)
		{
			_Check.GetInstance().SetResult(ConnectionMethod.WP, b);
			
		}

		public void CheckResult()
		{
			_Check.GetInstance().CheckResult(ConnectionMethod.WP, int.Parse(ConfigurationManager.AppSettings["wp_err_count"]));
		}

		#endregion
	}
}
