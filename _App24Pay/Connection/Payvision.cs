﻿using System;
using System.Configuration;
using System.Xml;

namespace _App24Pay.Connection
{
	public class Payvision:_IConnection
	{
		private _Connection_Util con = null;

		#region _IConnection メンバ

		public bool Run()
		{
			try
			{
				SetConnection_Util();

				//5006625（PV_EU_ECG） vm
				//5000439（PV_WHB_ECG） j
				con.mRequest_Data.Append("memberId=" + ConfigurationManager.AppSettings["pv_code"]);
				con.mRequest_Data.Append("&memberGuid=" + ConfigurationManager.AppSettings["pv_pass"]);
				con.mRequest_Data.Append("&countryId=392");
				con.mRequest_Data.Append("&amount=10");
				con.mRequest_Data.Append("&currencyId=392");
				con.mRequest_Data.Append("&trackingMemberCode=" + DateTime.Now.ToString("HHmmssfff"));
				con.mRequest_Data.Append("&cardNumber=4444333322221111");
				con.mRequest_Data.Append("&cardHolder=testupc");
				con.mRequest_Data.Append("&cardExpiryMonth=" + DateTime.Now.AddMonths(1).Month.ToString("00"));
				con.mRequest_Data.Append("&cardExpiryYear=" + DateTime.Now.AddMonths(1).Year.ToString());
				con.mRequest_Data.Append("&cardCvv=");	// 
				con.mRequest_Data.Append("&merchantAccountType=1");
				con.mRequest_Data.Append("&cardType=");
				con.mRequest_Data.Append("&issueNumber=");
				con.mRequest_Data.Append("&dynamicDescriptor=");
				con.mRequest_Data.Append("&avsAddress=");
				con.mRequest_Data.Append("&avsZip=");

				con.ConnectHttpRequest();


				bool ret = !string.IsNullOrEmpty(con.mResponse_Xml_Data.InnerXml);

				if (ret && con.StatusCode != System.Net.HttpStatusCode.OK)
				{
					ret = false;
				}

				//ここまできて中身チェック
				if (ret)
				{
					XmlNodeList nodes = con.mResponse_Xml_Data["TransactionResult"]["Cdc"].GetElementsByTagName("CdcEntry");
					string bankCode = "";
					for (int i = 0; i < nodes.Count; i++)
					{
						if (nodes[i]["Name"].InnerText == "BankInformation")
						{
							XmlNodeList nodes2 = nodes[i]["Items"].GetElementsByTagName("CdcEntryItem");
							for (int j = 0; j < nodes2.Count; j++)
							{
								if (nodes2[i]["Key"].InnerText == "BankCode")
								{
									bankCode = nodes2[i]["Value"].InnerText;
									break;
								}
							}
							break;
						}
					}
					//CdcEntryは複数ある。
					if (bankCode == "19" || bankCode == "68") ret = false;
				}
				SetResult(ret);
				CheckResult();
				return ret;
			}
			catch (Exception ex)
			{
				SetResult(false);
				CheckResult();
				return false;
			}
		}

		public void SetConnection_Util()
		{
			con = new _Connection_Util();
			con.mRequest_Url = ConfigurationManager.AppSettings["pv_url"];
			con.mRequest_Type = ConnectionRequestType.Post;
			con.mRequest_Option = ConnectionRequestOption.Normal;
			con.mResponse_Option = ConnectionResponseOption.XmlData;
			con.mRequest_User = "";
			con.mRequest_Pass = "";
			con.mTime_Out = 10000;
		}

		#endregion

		

		#region _IConnection メンバ


		public void SetResult(bool b)
		{

			_Check.GetInstance().SetResult(ConnectionMethod.PV, b);
		}

		#endregion

		#region _IConnection メンバ


		public void CheckResult()
		{

			_Check.GetInstance().CheckResult(ConnectionMethod.PV, int.Parse(ConfigurationManager.AppSettings["pv_err_count"]));
		}

		#endregion
	}
}
