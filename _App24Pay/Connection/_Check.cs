﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace _App24Pay.Connection
{
	public class _Check
	{
		private static _Check _check = new _Check();
		public static _Check GetInstance()
		{
			return _check;
		}
		private _Check() { }


		public void SetResult(ConnectionMethod method, bool bResult)
		{
			string filepath = GetLogpath(method, DateTime.Now);
			string text = DateTime.Now.ToString() + "," + (bResult ? "o" : "x") + Environment.NewLine;
			File.AppendAllText(filepath, text, Encoding.UTF8);
		}

		public bool CheckResult(ConnectionMethod method, int nErr_Count)
		{

			string filepath_n = GetLogpath(method, DateTime.Now);


			string[] now = File.ReadAllLines(filepath_n, Encoding.UTF8);
			string[] yesterday = null;
			if (now.Length < nErr_Count)
			{
				string filepath_y = GetLogpath(method, DateTime.Now.AddDays(-1));

				if (!File.Exists(filepath_y)) return true;//成功とするしかない。

				yesterday = File.ReadAllLines(filepath_y, Encoding.UTF8);
			}

			string[] check;
			if (yesterday == null)
			{//nowだけで
				check = now;
			}
			else
			{
				check = new string[now.Length + yesterday.Length];
				yesterday.CopyTo(check, 0);
				now.CopyTo(check, yesterday.Length);
			}

			bool result = false;
			for (int i = (check.Length - nErr_Count); i < check.Length; i++)
			{
				string[] line = check[i].Split(',');
				if (line.Length != 2)
				{
					i--;
					continue;
				}
				if (line[1] == "o")
				{
					result = true;
					break;
				}
			}

			string tmp = "_check_";
			switch (method)
			{
				case ConnectionMethod.WP:
					tmp += "wp";
					break;
				case ConnectionMethod.PV:
					tmp += "pv";
					break;
				case ConnectionMethod.AW:
					tmp += "aw";
					break;
			}

			string check_file_path = Path.GetDirectoryName(filepath_n);
			check_file_path = Path.Combine(check_file_path, DateTime.Now.ToString("yyyyMMdd") + tmp + ".log");


			//////////////////////////
			File.AppendAllText(check_file_path, DateTime.Now + ":" + result + Environment.NewLine);
			//////////////////////////


			return result;
		}


		private string GetLogpath(ConnectionMethod method, DateTime dt)
		{
			string tmp = "";
			switch (method)
			{
				case ConnectionMethod.WP:
					tmp += "wp";
					break;
				case ConnectionMethod.PV:
					tmp += "pv";
					break;
				case ConnectionMethod.AW:
					tmp += "aw";
					break;
				default:
				case ConnectionMethod.None:
					return "";
			}
			string filename = dt.ToString("yyyyMMdd_") + tmp + ".log";

			string log_dir = ConfigurationManager.AppSettings["log_dir"];
			log_dir = Path.Combine(log_dir, tmp);
			if (!Directory.Exists(log_dir)) Directory.CreateDirectory(log_dir);

			string filepath = Path.Combine(log_dir, filename);
			return filepath;
		}
	}
}