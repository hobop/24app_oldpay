﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;

namespace _App24Pay.Connection
{
	public class AlliedWallet:_IConnection
	{
		private _Connection_Util con = null;

		#region _IConnection メンバ

		public bool Run()
		{
			try
			{
				SetConnection_Util();

				
				XmlElement elm = null;
				string data_dir = ConfigurationManager.AppSettings["data_dir"];
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.XmlResolver = null;
				xmlDoc.Load(Path.Combine(data_dir, "aw.xml"));


				elm = xmlDoc["soap:Envelope"]["soap:Body"]["ExecuteCreditCard2"]["ExpiryYear"];
				elm.InnerText = DateTime.Now.AddMonths(1).Year.ToString();

				elm = xmlDoc["soap:Envelope"]["soap:Body"]["ExecuteCreditCard2"]["ExpiryMonth"];
				elm.InnerText = DateTime.Now.AddMonths(1).Month.ToString("00");

				elm = xmlDoc["soap:Envelope"]["soap:Body"]["ExecuteCreditCard2"]["MerchantReference"];
				elm.InnerText = DateTime.Now.ToString("HHmmssfff");


				con.mRequest_Data = new System.Text.StringBuilder(xmlDoc.InnerXml);
				con.ConnectHttpRequest();

				bool ret = !string.IsNullOrEmpty(con.mResponse_Xml_Data.InnerXml);

				if (ret && con.StatusCode != System.Net.HttpStatusCode.OK)
				{
					ret = false;
				}

				//ここまできて中身チェック
				if (ret)
				{
					try{
						if (con.mResponse_Xml_Data["soap:Envelope"]["soap:Body"]["ExecuteCreditCard2Response"]["Message"].InnerText.Contains("Cound Not Connect"))
						{
							ret = false;
						}
					}
					catch{}
				}
				SetResult(ret);
				CheckResult();
				return ret;
			}
			catch (Exception ex)
			{
				SetResult(false);
				CheckResult();
				return false;
			}

		}

		public void SetConnection_Util()
		{
			con = new _Connection_Util();
			con.mRequest_Url = "https://service.381808.com/Merchant.asmx?op=ExecuteCreditCard2";
			con.mRequest_Type = ConnectionRequestType.Post;
			con.mRequest_Option = ConnectionRequestOption.XmlData;
			con.mResponse_Option = ConnectionResponseOption.XmlData;
		}

		#endregion

		#region _IConnection メンバ


		public void SetResult(bool b)
		{
			_Check.GetInstance().SetResult(ConnectionMethod.AW, b);
			
		}

		public void CheckResult()
		{
			_Check.GetInstance().CheckResult(ConnectionMethod.AW , int.Parse(ConfigurationManager.AppSettings["aw_err_count"]));
		}

		#endregion
	}
}
