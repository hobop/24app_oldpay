﻿using System;
using _App24Pay.Connection;

namespace _App24Pay
{
	class Program
	{
		static void Main(string[] args)
		{
			ConnectionMethod method = ConnectionMethod.None;
			try
			{
				method = (ConnectionMethod)(int.Parse(args[0]));
			}
			catch
			{
				return;
			}

			switch (method)
			{
				default:
				case ConnectionMethod.None:
					throw new Exception();
				case ConnectionMethod.WP:
					Worldpay wp = new Worldpay();
					wp.Run();
					break;
				case ConnectionMethod.PV:
					Payvision pv = new Payvision();
					pv.Run();
					break;
				case ConnectionMethod.AW:
					AlliedWallet aw = new AlliedWallet();
					aw.Run();
					break;
			}
		}
	}
}
